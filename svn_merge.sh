#!/bin/bash
# Usage: svn_merge {trunk url} {branch url}
# This script requires 2 parameters. The first one is the full url of the trunk and the second is the full url of the branch. Do not add trailing slashes in the urls.
TRUNK=$1
BRANCH=$2
TMP_DIR=~/svn/temp_merge_dir/
BRANCH_DIR=`echo $BRANCH | rev | cut -d '/' -f 1 | rev`
MERGED_DIR="MERGED_"$BRANCH_DIR
TRUNK_DIR=`echo $TRUNK | rev | cut -d '/' -f 1 | rev`
NEW_BRANCH=${BRANCH/$BRANCH_DIR/$MERGED_DIR}
SVN_USER=$(whoami) # Change to your SVN username
SVN_PASS="Add your SVN password"
CHECK_TRUNK=$(svn info $TRUNK)
CHECK_BRANCH=$(svn info $BRANCH)

# Setting your credentials to access the repo
svn auth --username $SVN_USER --password $SVN_PASS # add this line to ~/.bashrc ~/.zshrc and prepend alias svn_auth= to it. Then use it just as svn_auth. Don't forget to declare and export SVN_USER and SVN_PASS also.

echo "$SVN_USER Authenticated successfully"

# First, create a temp folder to work and manage working copies
CURRENT_DIR=$(pwd)
BASE_DIR=$CURRENT_DIR
mkdir -p $TMP_DIR
cd $TMP_DIR
echo "$TMP_DIR temporary folder created"

if [[ -z $CHECK_TRUNK ]]; then
    echo "The trunk $TRUNK doesn't exists"
else
  # Second, download the trunk to the temp folder
  svn checkout $TRUNK
  cd $TRUNK_DIR
  CURRENT_DIR=$(pwd)

  if [$CURRENT_DIR == $BASE_DIR/$TRUNK_DIR]
  then
    echo "Trunk downloaded and accessed successfully"

    if [[ -z $CHECK_BRANCH ]]; then
        echo "The branch $BRANCH doesn't exists"
    else
      # Third, merge the branch into the trunk
      svn merge $BRANCH
      svn commit -m "Merge executed from $BRANCH to $TRUNK"
      echo "Merge and commit the branch $BRANCH_DIR into trunk $TRUNK"

      # Fourth, rename the branch to flag it as merged
      svn mv $BRANCH $NEW_BRANCH
      svn commit -m "Branch renamed and flagged as merged"
      echo "Rename and flag the branch as merged"
    fi
  fi
fi

# Finally, exit the temporary folder and delete it
cd $BASE_DIR
sudo rm -rf $TMP_DIR
echo "Exiting the temporary folder and completing deletion."
